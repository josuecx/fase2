﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using diproma.Clases;
namespace diproma.Paginas
{
    public partial class crudkilometraje : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from kilometraje";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
             if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla()
        {
            string cred = "Select * from kilometraje";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar()
        {
            bus.Text = "";
            placa.Text = "";
            id_kilometraje.Text = "";
            kilometraje.Text = "";
        }
        protected void crea_Click(object sender, EventArgs e)
        {
            string pl = "" + placa.Text;
            int dk = Convert.ToInt32(id_kilometraje.Text);
            int k = Convert.ToInt32(kilometraje.Text);
            string ms= mes.SelectedValue;
            string a = "select * from kilometraje where id_kilometraje=" + dk + " and mes='"+ms+"' and placa='"+pl+"'";
            string a1 = "select * from kilometraje where id_kilometraje=" + dk + " ";
            if (conn.busqueda(a).Rows.Count > 0)//kilometraje y mes y placa
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Kilometraje, placa y mes, ya registrado')", true);
            }
            else if (conn.busqueda(a1).Rows.Count > 0) {// ide se repite hay error  
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Kilometraje ya registrado')", true);
            }
            else
            {
                string a2 = "select * from kilometraje where mes='" + ms + "' and placa='"+pl+"'";
                if (conn.busqueda(a).Rows.Count > 0) {//no placa mes 
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error mes y placa')", true);
                }
                else {
                    string llenar1 = " insert into kilometraje(id_kilometraje,mes,kilometraje,placa) values(" + k + " , '" + ms + "' , " + k + " , '" + pl + "'  )";
                    try
                    {
                        conn.Operacion(llenar1);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro kilometraje')", true);
                    }
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                string pl = "" + placa.Text;
                int bus1 = Convert.ToInt32(bus.Text);
                int dk = Convert.ToInt32(id_kilometraje.Text);
                int k = Convert.ToInt32(kilometraje.Text);
                string ms = mes.SelectedValue;
                if (bus1==dk)
                {
                    string a = "select * from kilometraje where mes='"+ms+"' and placa='"+pl+"' ";
                    if (conn.busqueda(a).Rows.Count > 0)
                    {// solo kilo
                        string llenar2 = "Update kilometraje SET  kilometraje=" + k + "  where id_kilometraje=" + bus1 + " ";
                        try
                        {
                            conn.Operacion(llenar2);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado kilometaje')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al agregar nuevo kilometraje')", true);
                        }
                    }
                    else {
                        string llenar1 = "Update kilometraje SET mes='" + ms + "' , kilometraje=" + k + " , placa='"+pl+"' where id_kilometraje=" + bus1 + " ";
                        try
                        {
                            conn.Operacion(llenar1);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de modificar Kilometraje, mes, placa')", true);
                        }
                    }
                }
                else
                {
                    string a1 = "select * from kilometraje where id_kilometraje=" + dk + " ";
                    if (conn.busqueda(a1).Rows.Count > 0)//kilometraje y mes y placa
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Kilometraje, placa y mes, ya registrado')", true);
                    }
                    else 
                    {
                        
                            string a2 = "select * from kilometraje where mes='" + ms + "' and placa='" + pl + "' ";
                            if (conn.busqueda(a2).Rows.Count > 0)
                            {// solo kilo
                                string llenar2 = "Update kilometraje SET  id_kilometraje=" + dk + " , kilometraje=" + k + "  where id_kilometraje=" + bus1 + " ";
                                try
                                {
                                    conn.Operacion(llenar2);
                                    limpiar();
                                    datatabla();
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado kilometaje')", true);
                                }
                                catch
                                {
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al agregar nuevo id_kilo, kilometraje')", true);
                                }
                            }
                            else
                            {
                                string llenar1 = "Update kilometraje SET id_kilometraje=" + dk + " , mes='" + ms + "' , kilometraje=" + k + " , placa='" + pl + "'  where id_kilometraje=" + bus1 + " ";
                                try
                                {
                                    conn.Operacion(llenar1);
                                    limpiar();
                                    datatabla();
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                                }
                                catch
                                {
                                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error todo')", true);
                                }
                            }
                        
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int idbusqueda = Convert.ToInt32(bus.Text);
                string vtempleado = " select fecha from asignacion_vehiculo where fecha=" + idbusqueda + " ";
                if (conn.busqueda(vtempleado).Rows.Count > 0)
                {//credencial registrada en otra tabla
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error /n Para borrar tiene que borar el registro de Asignacion de vehiculo, vuelva a intentar')", true);
                }
                else
                {
                    string eliminar = "Delete from kilometraje where id_kilometraje=" + idbusqueda + " ";
                    try
                    {
                        conn.Operacion(eliminar);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error la placa no esta registrada')", true);
                    }
                }
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from kilometraje where id_kilometraje=" + idbusqueda + " ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from kilometraje where id_kilometraje=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    placa.Text = sdr["placa"].ToString();
                    id_kilometraje.Text = sdr["id_kilometraje"].ToString();
                    kilometraje.Text = sdr["kilometraje"].ToString();
                    
                }

            }
            else
            {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }
    }
}