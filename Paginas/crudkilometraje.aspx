﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crudkilometraje.aspx.cs" Inherits="diproma.Paginas.crudkilometraje" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Asignacion de Kilometraje
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    Asignacion de Kilometraje
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">

            <li><a href="crudvehiculos.aspx">CRUD Vehiculo</a></li>
            <li><a href="crudkilometraje.aspx">CRUD Kilometraje</a></li>
            <li><a href="crudasigvehiculo.aspx">Asignacion vehiculos</a></li>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" style="width:30px; height:5px; position:absolute; top: 3%; left: 60%;" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="placa" DataSourceID="vehi" ForeColor="Black" GridLines="Vertical">
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <Columns>
            <asp:BoundField DataField="placa" HeaderText="placa" ReadOnly="True" SortExpression="placa" />
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />

    </asp:GridView>
    <asp:SqlDataSource ID="vehi" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT [placa] FROM [vehiculo]"></asp:SqlDataSource>
    <table style="width: 108%; height: 366px;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">CRUD Kilometraje</h3></th>
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">IDE Kilometraje:</label>
            </td>
            <td>
                <asp:TextBox ID="id_kilometraje" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="id_kilometraje" Display="Dynamic" Text="* llenar campo"></asp:RequiredFieldValidator>
            </td>  
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Mes:</label>
            </td>
            <td>
                <asp:DropDownList ID="mes" runat="server" Width="128px">
                    <asp:ListItem>enero</asp:ListItem>
                    <asp:ListItem>febrero</asp:ListItem>
                    <asp:ListItem>marzo</asp:ListItem>
                    <asp:ListItem>abril</asp:ListItem>
                    <asp:ListItem>mayo</asp:ListItem>
                    <asp:ListItem>junio</asp:ListItem>
                    <asp:ListItem>julio</asp:ListItem>
                    <asp:ListItem>agosto</asp:ListItem>
                    <asp:ListItem>septiembre</asp:ListItem>
                    <asp:ListItem>octubre</asp:ListItem>
                    <asp:ListItem>noviembre</asp:ListItem>
                    <asp:ListItem>diciembre</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
           <td style="width: 167px">
                <label class="d1">Kilometraje:</label>
            </td>
            <td>
                <asp:TextBox ID="kilometraje" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="kilometraje" Display="Dynamic" ErrorMessage="RequiredFieldValidator">*Solo numero</asp:RequiredFieldValidator>
            </td>
        </tr>
          <tr>
            <td style="width: 167px">
                <label class="d1">Placa:</label>
            </td>
            <td>
                <asp:TextBox  ID="placa" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="placa" Display="Dynamic" Text="* Llenar Campo"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:Button ID="crea" runat="server" Text="Crear" Height="36px" Width="121px" BackColor="Lime" BorderStyle="Solid" OnClick="crea_Click" />
            </td>
            <td>
                <asp:Button ID="mo" runat="server" Text="Modificar" Height="32px" Width="151px" BackColor="#CC0099" BorderStyle="Solid" OnClick="mo_Click" />
            </td>
            <td>
                 <asp:Button ID="eli" runat="server" Text="Eliminar" Height="30px" Width="97px" BackColor="Red" BorderStyle="Solid" OnClick="eli_Click" />
            </td>   
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:TextBox ID="bus" runat="server" Height="25px" Width="132px"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="buscar" runat="server" Text="Buscar" Height="37px" Width="149px" BackColor="Aqua" BorderStyle="Solid" OnClick="buscar_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvVe" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />

    </asp:GridView>
</asp:Content>
