﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using diproma.Clases;

namespace diproma.Paginas
{
    
    public partial class crud_ciudad : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from ciudad";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
            if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla()
        {
            string cred = "Select * from ciudad";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar()
        {
            codigo.Text = "";
            nombre.Text = "";
        }
        protected void crea_Click(object sender, EventArgs e)
        {
            int cod = Convert.ToInt32(codigo.Text);
            int ca = Convert.ToInt32(depto.SelectedValue);
            string n = nombre.Text;
            string a1 = "select * from ciudad where codigo=" + cod + " ";
            if (conn.busqueda(a1).Rows.Count > 0)//codigo
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo')", true);
            }
            else
            {
                string llenar1 = " insert into ciudad(codigo,nombre,cod_depto) values(" + cod + " , '" + n + "' , "+ca+" )";
                try
                {
                    conn.Operacion(llenar1);
                    limpiar();
                    datatabla();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                }
                catch
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro ciudad')", true);
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int cod = Convert.ToInt32(codigo.Text);
                int ca = Convert.ToInt32(depto.SelectedValue);
                int bus1 = Convert.ToInt32(bus.Text);
                string n = nombre.Text;
                if (bus1 == cod)
                {
                    string llenar1 = "Update ciudad SET nombre='" + n + "' , cod_depto=" + ca + " where codigo=" + bus1 + "";
                    try
                    {
                        conn.Operacion(llenar1);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de modificar ciudad')", true);
                    }
                }
                else
                {
                    string a1 = "select * from ciudad where codigo=" + cod + " ";
                    if (conn.busqueda(a1).Rows.Count > 0)//kilometraje y mes y placa
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo de ciudad, ya registrado')", true);
                    }
                    else
                    {
                        string llenar2 = "Update ciudad SET  codigo=" + cod + " , nombre='" + n + "' cod_depto=" + ca + " where codigo=" + bus1 + "  ";
                        try
                        {
                            conn.Operacion(llenar2);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado producto')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al modificar nueva ciudad')", true);
                        }
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int idbusqueda = Convert.ToInt32(bus.Text);
                //restriccion de lista
                string vtempleado = " select cod_ciudad from cliente where cod_ciudad=" + idbusqueda + " ";
                if (conn.busqueda(vtempleado).Rows.Count > 0)
                {//credencial registrada en otra tabla
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error /n Para borrar tiene que borar el registro al cliente, vuelva a intentar')", true);
                }
                else
                {
                    string eliminar = "Delete from ciudad where codigo=" + idbusqueda + " ";
                    try
                    {
                        conn.Operacion(eliminar);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error ciudad')", true);
                    }
                }
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from ciudad where codigo=" + idbusqueda + " ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from ciudad where codigo=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    nombre.Text = sdr["nombre"].ToString();
                    codigo.Text = sdr["codigo"].ToString();
                }
            }
            else
            {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }
    }
}