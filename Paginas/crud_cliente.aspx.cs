﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using diproma.Clases;
namespace diproma.Paginas
{
    public partial class crud_cliente : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from cliente";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
            if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla()
        {
            string cred = "Select * from cliente";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar()
        {
            nit.Text = "";
            nombre.Text = "";
            apellido.Text = "";
            nacimiento.Text = "";
            direccion.Text = "";
            telefono.Text = "";
            celular.Text = "";
            limite.Text = "";
            dcredito.Text = "";
        }
        protected void crea_Click(object sender, EventArgs e)
        {
            int nt = Convert.ToInt32(nit.Text);
            int lm = Convert.ToInt32(limite.Text);
            int cre = Convert.ToInt32(dcredito.Text);
            int tel= Convert.ToInt32(telefono.Text);
            int cel= Convert.ToInt32(celular.Text);
            string n = nombre.Text;
            string ape = apellido.Text;
            string nac = nacimiento.Text;
            string dire = direccion.Text;
            string lis1 = "" + cod_lista.SelectedItem.Text;
            string[] codl = lis1.Split('-');
            string lis2 = "" + ciudad.SelectedItem.Text;
            string[] cid = lis2.Split('-');
            string lis3 = "" + departamento.SelectedItem.Text;
            string[] dep = lis3.Split('-');
            string em = email.Text;
            string a1 = "select * from cliente where nit=" + nt + " ";
            if (conn.busqueda(a1).Rows.Count > 0)//codigo
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Cliente ya registrado')", true);
            }
            else
            {
                string llenar1 = " insert into cliente(nit,nombres,apellidos,nacimiento,direccion,telefono,celular,email,cod_ciudad,cod_depto1,limite_credito,dias_credito,codigo_lista) " +
                    " values(" + nt + " , '" + n + "' , '" + ape + "' , '" + nac+ "' , '" + dire + "' , "+tel+" , "+cel+",'"+em+"' , '"+cid[0]+"', '"+dep[0]+"',"+lm+","+cre+",'"+codl[0]+"')";
                try
                {
                    conn.Operacion(llenar1);
                    limpiar();
                    datatabla();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                }
                catch
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro kilometraje')", true);
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int idbusqueda = Convert.ToInt32(bus.Text);
                //restriccion de lista
                string vtempleado = " select codigo_producto from producto where fecha=" + idbusqueda + " ";
                if (conn.busqueda(vtempleado).Rows.Count > 0)
                {//credencial registrada en otra tabla
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error /n Para borrar tiene que borar el registro de Asignacion de vehiculo, vuelva a intentar')", true);
                }
                else
                {
                   
                }
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from producto where codigo_producto=" + idbusqueda + " ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from producto where codigo_producto=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                   
                }
            }
            else
            {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }
    }
}