﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using diproma.Clases;
namespace diproma.Paginas
{
    public partial class crud_producto : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from producto";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
            if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla()
        {
            string cred = "Select * from producto";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar()
        {
            codigo.Text = "";
            nombre.Text = "";
            descripcion.Text = "";
            imagen.Text = "";
        }
        protected void crea_Click(object sender, EventArgs e)
        {
            int cod = Convert.ToInt32(codigo.Text);
            int ca = Convert.ToInt32(categoria.SelectedValue);
            string n = nombre.Text;
            string im = imagen.Text;
            string ds= descripcion.Text;
            string a1 = "select * from producto where codigo_producto=" + cod + " ";
            if (conn.busqueda(a1).Rows.Count > 0)//codigo
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo')", true);
            }
            else
            {
                    string llenar1 = " insert into producto(codigo_producto,nombre,descripcion,imagen,categoria1) values(" + cod+ " , '" + n + "' , '" + im + "' , '" + im + "' , "+ca+" )";
                    try
                    {
                        conn.Operacion(llenar1);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro kilometraje')", true);
                    }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int cod = Convert.ToInt32(codigo.Text);
                int ca = Convert.ToInt32(categoria.SelectedValue);
                int bus1 = Convert.ToInt32(bus.Text);
                string n = nombre.Text;
                string im = imagen.Text;
                string ds = descripcion.Text;
                if (bus1 == cod)
                {
                        string llenar1 = "Update producto SET nombre='" + n + "' , descripcion='"+ds+"' , imagen='"+ im +"'  , categoria1=" + ca + " where codigo_producto="+bus1+"";
                        try
                        {
                            conn.Operacion(llenar1);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de modificar producto')", true);
                        }
                }
                else
                {
                    string a1 = "select * from producto where codigo_producto=" + cod + " ";
                    if (conn.busqueda(a1).Rows.Count > 0)//kilometraje y mes y placa
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo de producto, ya registrado')", true);
                    }
                    else
                    {
                            string llenar2 = "Update producto SET  codigo_producto=" + cod + " , nombre='" + n + "' , descripcion='" + ds + "' , imagen='" + im + "'  , categoria1=" + ca + " where codigo_producto=" + bus1 + "  ";
                            try
                            {
                                conn.Operacion(llenar2);
                                limpiar();
                                datatabla();
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado producto')", true);
                            }
                            catch
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al agregar nuevo producto')", true);
                            }
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int idbusqueda = Convert.ToInt32(bus.Text);
                //restriccion de lista
                string vtempleado = " select codigo_producto from producto where codigo_producto=" + idbusqueda + " ";
                if (conn.busqueda(vtempleado).Rows.Count > 0)
                {//credencial registrada en otra tabla
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error /n Para borrar tiene que borar el registro de Asignacion de vehiculo, vuelva a intentar')", true);
                }
                else
                {
                    string eliminar = "Delete from producto where codigo_producto=" + idbusqueda + " ";
                    try
                    {
                        conn.Operacion(eliminar);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error la placa no esta registrada')", true);
                    }
                }
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from producto where codigo_producto=" + idbusqueda + " ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from producto where codigo_producto=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    nombre.Text = sdr["nombre"].ToString();
                    codigo.Text = sdr["codigo_producto"].ToString();
                    imagen.Text = sdr["imagen"].ToString();
                    descripcion.Text = sdr["descripcion"].ToString();
                }
            }
            else
            {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }
    }
}