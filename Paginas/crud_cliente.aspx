﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crud_cliente.aspx.cs" Inherits="diproma.Paginas.crud_cliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Crud Cliente
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    CRUD Cliente
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">
              <li><a href="crud_cliente.aspx">Cliente</a></li>
            <li><a href="crud_ciudad.aspx">Ciudad</a></li>
            <li><a href="crud_depto.aspx">Departamento</a></li>
            <li><a href="crud_meta.aspx">Metas</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">
    <table style="width: 108%; height: 366px;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">CRUD Cliente</h3></th>
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Nit Cliente:</label>
            </td>
            <td>
                <asp:TextBox ID="nit" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="nit" Display="Dynamic" Text="* llenar campo"></asp:RequiredFieldValidator>
            </td>  
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Nombres:</label>
            </td>
            <td>
                <asp:TextBox ID="nombre" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
           <td style="width: 167px">
                <label class="d1">Apellidos:</label>
            </td>
            <td>
                <asp:TextBox ID="apellido" runat="server"></asp:TextBox>

            </td>
        </tr>
          <tr>
            <td style="width: 167px">
                <label class="d1">Fecha Nacimiento:</label>
            </td>
            <td>
                <asp:TextBox ID="nacimiento" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Direccion:</label>
            </td>
            <td>
                <asp:TextBox  ID="direccion" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td style="width: 167px">
                <label class="d1">Telefono:</label>
            </td>
            <td>
                <asp:TextBox  ID="telefono" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td style="width: 167px">
                <label class="d1">Celular:</label>
            </td>
            <td>
                <asp:TextBox  ID="celular" runat="server"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td style="width: 167px">
                <label class="d1">Email:</label>
            </td>
            <td>
                <asp:TextBox  ID="email" runat="server"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td style="width: 167px">
                <label class="d1">Departamento:</label>
            </td>
            <td>
                <asp:DropDownList ID="departamento" runat="server" DataSourceID="d1" DataTextField="codigo" DataValueField="codigo" Height="16px" Width="127px"></asp:DropDownList>
                <asp:SqlDataSource ID="d1" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT CAST(codigo as VARCHAR) + '-' + CAST(nombre as VARCHAR) as cyn from departamento"></asp:SqlDataSource>
            </td>
        </tr>
         <tr>
            <td style="width: 167px">
                <label class="d1">Ciudad:</label>
            </td>
            <td>
                <asp:DropDownList ID="ciudad" runat="server" DataSourceID="c1" DataTextField="codigo" DataValueField="codigo" Height="16px" Width="125px"></asp:DropDownList>
                <asp:SqlDataSource ID="c1" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT CAST(codigo as VARCHAR) + '-' + CAST(nombre as VARCHAR) as cyn from ciudad"></asp:SqlDataSource>
            </td>
        </tr>
         <tr>
            <td style="width: 167px">
                <label class="d1">Limite de Credito:</label>
            </td>
            <td>
                <asp:TextBox  ID="limite" runat="server" TextMode="Number"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td style="width: 167px">
                <label class="d1">Dias de credito:</label>
            </td>
            <td>
                <asp:TextBox  ID="dcredito" runat="server" TextMode="Number"></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td style="width: 167px">
                <label class="d1">Codigo de lista:</label>
            </td>
            <td>
                <asp:DropDownList ID="cod_lista" runat="server" DataSourceID="cl" DataTextField="cyn" DataValueField="cyn" Height="16px" Width="130px"></asp:DropDownList>
                <asp:SqlDataSource ID="cl" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT CAST(codigol as VARCHAR) + '-' + CAST(nombre as VARCHAR) as  cyn from  lista_producto"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:Button ID="crea" runat="server" Text="Crear" Height="54px" Width="154px" BackColor="Lime" BorderStyle="Solid" OnClick="crea_Click" />
            </td>
            <td>
                <asp:Button ID="mo" runat="server" Text="Modificar" Height="47px" Width="153px" BackColor="#CC0099" BorderStyle="Solid" OnClick="mo_Click" />
            </td>
            <td>
                 <asp:Button ID="eli" runat="server" Text="Eliminar" Height="51px" Width="104px" BackColor="Red" BorderStyle="Solid" OnClick="eli_Click" />
            </td>   
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:TextBox ID="bus" runat="server" Height="25px" Width="132px" TextMode="Number"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="buscar" runat="server" Text="Buscar" Height="48px" Width="148px" BackColor="Aqua" BorderStyle="Solid" OnClick="buscar_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvVe" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />

    </asp:GridView>
</asp:Content>
