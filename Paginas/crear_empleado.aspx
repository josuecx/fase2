﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crear_empleado.aspx.cs" Inherits="diproma.Paginas.crear_empleado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    CRUD Empleado
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    CRUD Empleado 
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="submenu" runat="server">
    
            <li><a href="../Paginas/crear_empleado.aspx">CRUD Empleado</a></li>
            <li><a href="../Paginas/ver_empleado.aspx">Ver Empleado</a></li>
            <li><a href="cargaempleado.aspx">Cargar Empleado</a></li>
            <li><a href="asigempleado.aspx">Asignar Empleados</a></li>
            <li><a href="crud_credencial.aspx">CRUD Credenciales</a></li>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cuadro1" runat="server">
    <asp:GridView ID="GridView2" CssClass="tablapuesto" style="width:30px; height:5px; position:absolute; top: 3%; left: 60%;" runat="server" DataSourceID="SqlDataSource3" AutoGenerateColumns="False" DataKeyNames="id_credencial" AllowPaging="True" CellPadding="4" ForeColor="#333333" GridLines="None">
         <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
         <Columns>
             <asp:BoundField DataField="id_credencial" HeaderText="id_credencial" ReadOnly="True" SortExpression="id_credencial" />
             <asp:BoundField DataField="credencial" HeaderText="credencial" SortExpression="credencial" />
         </Columns>
         <EditRowStyle BackColor="#999999" />
         <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
         <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
         <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
         <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
         <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
         <SortedAscendingCellStyle BackColor="#E9E7E2" />
         <SortedAscendingHeaderStyle BackColor="#506C8C" />
         <SortedDescendingCellStyle BackColor="#FFFDF8" />
         <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString='<%$ ConnectionStrings:dipromaConnectionString %>' SelectCommand="SELECT * FROM [credencial] ORDER BY [id_credencial]"></asp:SqlDataSource>
    <table class="auto-style38" style="width: 666px; height: 671px;">
                <tr>
                    <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">Crear Empleado</h3></th>
                </tr>
                  <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Nit del empleado:</label></td>
                     <td><asp:TextBox ID="nit" runat="server" CssClass="hel" placeholder="Escriba el nit" Width="131px"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="nit" Display="Dynamic" ErrorMessage="* LLenar campo"></asp:RequiredFieldValidator>
                         <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="nit" Display="Dynamic" ErrorMessage="Solo Numeros" MaximumValue="9999999999999" MinimumValue="0"></asp:RangeValidator>  
                     </td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Nombres:</label></td>
                     <td><asp:TextBox ID="nombres" runat="server" CssClass="hel" placeholder="Escriba sus nombres" Width="182px"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Error" Display="Dynamic" Text="* llenar campo" ControlToValidate="nombres"></asp:RequiredFieldValidator>
                     </td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Apellidos:</label></td>
                     <td><asp:TextBox ID="apellidos" runat="server" CssClass="hel" placeholder="Escriba sus apellidos" Width="180px"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Error" ControlToValidate="apellidos" Display="Dynamic" Text="* Llenar campo"></asp:RequiredFieldValidator>
                     </td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Fecha de Nacimiento:</label></td>
                     <td><asp:TextBox ID="fecha_nac" runat="server" CssClass="hel" placeholder="Escriba fecha nacimiento" Width="179px" TextMode="Date"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="* Error" ControlToValidate="fecha_nac" Display="Dynamic" Text="* Llenar campo"></asp:RequiredFieldValidator>
                     </td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Domicilio:</label></td>
                     <td><asp:TextBox ID="domicilio" runat="server" CssClass="hel" placeholder="Escriba el domicilio" Width="177px"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="* Error" ControlToValidate="domicilio" Display="Dynamic" Text="* Llenar campo"></asp:RequiredFieldValidator>  
                     </td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Telefono Domiciliar:</label></td>
                     <td><asp:TextBox ID="telefono_d" runat="server" CssClass="hel" placeholder="Escribe numero telefonico" Width="133px" TextMode="Number"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Numero Celular:</label></td>
                     <td><asp:TextBox ID="celular" runat="server" CssClass="hel" placeholder="Escriba numero celular" Width="132px" TextMode="Number"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Correo Electronico:</label></td>
                     <td><asp:TextBox ID="email" runat="server" CssClass="hel" placeholder="Escribe el correo electronico" Width="179px" TextMode="Email"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="* Error" ControlToValidate="email" Display="Dynamic" Text="* Llenar campo"></asp:RequiredFieldValidator>
                     </td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Password:</label></td>
                     <td><asp:TextBox ID="pass" runat="server" CssClass="hel" placeholder="Escriba el password" Width="180px" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="* Error " ControlToValidate="pass" Display="Dynamic" Text="* Llenar campo"></asp:RequiredFieldValidator>
                     </td>
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Credencial o Puesto:</label></td>
                     <td>
                         <asp:DropDownList ID="credencial" runat="server" Height="20px" Width="187px" DataSourceID="puesto" DataTextField="id_credencial" DataValueField="id_credencial" ></asp:DropDownList>
                         <asp:SqlDataSource ID="puesto" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT [id_credencial], [credencial] FROM [credencial] ORDER BY [id_credencial]"></asp:SqlDataSource>
                       
                     </td>
                  
                 </tr>
                 <tr>
                     <td class="auto-style37" style="width: 252px"><label class="d1">Buscar nit:</label></td>
                     <td class="auto-style37">
                         <asp:DropDownList ID="buscarnit" runat="server" DataSourceID="nit_empleado" DataTextField="nit" DataValueField="nit" Width="190px"></asp:DropDownList>
                         <asp:SqlDataSource ID="nit_empleado" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT [nit] FROM [empleado]"></asp:SqlDataSource>
                         <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
                     </td>
                    <td class="auto-style37"><asp:Button ID="bus1" runat="server" Text="Buscar" OnClick="bus1_Click" BackColor="#66FFFF" BorderStyle="Solid" ForeColor="#003300" Height="38px" Width="153px" />
                        <asp:TextBox ID="busqueda_nit" runat="server" Width="141px"></asp:TextBox>
                       
                    </td>
                 </tr> 
               
                 <tr>
                     <td class="auto-style37" style="width: 252px"><asp:Button ID="crear" runat="server" Text="Crear Empleado" Height="40px" BorderStyle="Solid" OnClick="crear_Click" BackColor="#33CC33" /></td>
                     <td class="auto-style37"><asp:Button ID="mo" runat="server" Text="Modificar Empleado" Height="40px" OnClick="mo_Click" BackColor="#990099" /></td>
                     <td class="auto-style37"><asp:Button ID="eli" runat="server" Text="Eliminar Empleado" Height="40px" OnClick="eli_Click" BackColor="Red" /></td>
                 </tr>
                                
             </table>
    <asp:GridView ID="gvEmpleado" runat="server" style="width:30px; height:5px;" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
        
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />

    </asp:GridView>
     
</asp:Content>

  