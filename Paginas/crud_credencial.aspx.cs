﻿using diproma.Clases;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace diproma.Paginas
{
    public partial class crud_credencial : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from credencial";
            if (!IsPostBack) {
                //cargavisual();
                gvC.DataSource = conn.Dset(cred);
                gvC.DataBind();
            }
        }
        public void cargavisual() {
            string cred = "Select * from credencial";
            gvC.DataSource = conn.Dset(cred);
            gvC.DataBind();
        }

        protected void cre_Click(object sender, EventArgs e)
        {
            //var 
            int creden = Convert.ToInt32(id_cre.Text);
            string puesto = ncre.Text;
            string a = "select * from credencial where id_credencial="+creden+" and credencial='"+puesto+"' ";
            if (conn.busqueda(a).Rows.Count > 0)
            {// ya existe 
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error credencial ya registrado')", true);
            }
            else {
                string llenar1 = " insert into credencial(id_credencial,credencial) values("+creden+",'"+puesto+"')";
                try
                {
                    conn.Operacion(llenar1);
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                }
                catch
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro Credencial')", true);
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else {
                int idbuscar = Convert.ToInt32(bus.Text);
                int idcambiar = Convert.ToInt32(id_cre.Text);
                string puesto = ncre.Text;
                string cambio1 = "select * from credencial where id_credencial="+idcambiar+"";
                if (idbuscar == idcambiar)
                {
                    string llenar1 = " update credencial set credencial='" + puesto + "' where id_credencial=" + idbuscar + "";
                    try
                    {
                        conn.Operacion(llenar1);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ah Modificado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de modificar credencial Credencial')", true);
                    }
                }
                else {
                    string vnuevo = " select * from credencial where id_credencial="+idcambiar+" ";
                    if (conn.busqueda(vnuevo).Rows.Count > 0)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Ide credencial ya existe, vuelva a intentar')", true);
                    }
                    else {
                        string llenar1 = " update credencial set credencial='" + puesto + "' , id_credencial="+idcambiar+" where id_credencial=" + idbuscar + "";
                        try
                        {
                            conn.Operacion(llenar1);
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ah Modificado')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de modificar credencial Credencial')", true);
                        }
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text=="") {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else {
                int idbusqueda = Convert.ToInt32(bus.Text);
                string vtempleado = " select puesto from empleado where puesto=" + idbusqueda + " ";
                if (conn.busqueda(vtempleado).Rows.Count > 0)
                {//credencial registrada en otra tabla
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Usuario ya registrado, vuelva a intentar')", true);
                }
                else {
                    string eliminar = "Delete from credencial where id_credencial="+idbusqueda+"";
                    try
                    {
                        conn.Operacion(eliminar);
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error credencial no encontrada')", true);
                    }
                }
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from credencial where id_credencial="+idbusqueda+"";
            if (conn.busqueda(vv).Rows.Count > 0) {
               // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from credencial where id_credencial=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    id_cre.Text = sdr["id_credencial"].ToString();
                    ncre.Text = sdr["credencial"].ToString();
                }

            }
            else {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }
    }
}