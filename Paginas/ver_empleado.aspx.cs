﻿using diproma.Clases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace diproma.Paginas
{
    public partial class ver_empleado : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string empleado = "Select * from empleado";
            //isPostBack carga todo los datos es un metodo
            if (!IsPostBack)
            {
                gvEmpleado.DataSource = conn.Dset(empleado);
                gvEmpleado.DataBind();

            }
        }
    }
}