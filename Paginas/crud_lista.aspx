﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crud_lista.aspx.cs" Inherits="diproma.Paginas.crud_lista" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">
    
             <li><a href="crud_producto.aspx">CRUD Producto</a></li>
            <li><a href="crud_categoria.aspx">CRUD Categoria</a></li>
            <li><a href="crud_lista.aspx">CRUD Lista</a></li>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">
     <table style="width: 124%; height: 447px;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">CRUD Lista</h3></th>
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Codigo de lista:</label>
            </td>
            <td>
                <asp:TextBox ID="codigo" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="codigo" Display="Dynamic" Text="* llenar campo"></asp:RequiredFieldValidator>
            </td>  
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Nombre:</label>
            </td>
            <td>
                <asp:TextBox ID="nombre" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="nombre" Display="Dynamic" ErrorMessage="RequiredFieldValidator">*Llenar campo</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
           <td style="width: 167px">
                <label class="d1">Fecha inicio:</label>
            </td>
            <td>
                <asp:TextBox ID="f_inicio" runat="server" TextMode="Date"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="f_inicio" Display="Dynamic" ErrorMessage="RequiredFieldValidator">*llenar Campo</asp:RequiredFieldValidator>
            </td>
        </tr>
          <tr>
            <td style="width: 167px">
                <label class="d1">Fecha final:</label>
            </td>
            <td>
                <asp:TextBox ID="f_fin" runat="server" TextMode="Date"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="f_fin" Display="Dynamic" ErrorMessage="RequiredFieldValidator">*Llenar campo</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Detalle:</label>
            </td>
            <td>
                <asp:TextBox  ID="detalle" runat="server"></asp:TextBox>
            </td>
        </tr>
            <tr>
            <td style="width: 167px">
                <asp:DropDownList ID="producto" runat="server" Height="30px" Width="136px" DataSourceID="nproducto" DataTextField="CodigoyNombre" DataValueField="CodigoyNombre"></asp:DropDownList>
                <asp:SqlDataSource ID="nproducto" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT CAST(codigo_producto as VARCHAR) + '-' + CAST(nombre as VARCHAR) as CodigoyNombre from producto"></asp:SqlDataSource>
            </td>
            <td>
                
         <asp:TextBox ID="precio" runat="server" placeholder="Escriba el precio del producto" Width="136px"></asp:TextBox>
            </td>
                <td>
                    <asp:Button ID="add_producto" runat="server" Text="Agregar Producto" BackColor="Lime" BorderColor="#003300" BorderStyle="Solid" Height="42px" OnClick="add_producto_Click" />
                </td>
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:Button ID="crea" runat="server" Text="Crear" Height="54px" Width="154px" BackColor="Lime" BorderStyle="Solid" OnClick="crea_Click" />
            </td>
            <td>
                <asp:Button ID="mo" runat="server" Text="Modificar" Height="47px" Width="153px" BackColor="#CC0099" BorderStyle="Solid" OnClick="mo_Click" />
            </td>
            <td>
                 <asp:Button ID="eli" runat="server" Text="Eliminar" Height="51px" Width="104px" BackColor="Red" BorderStyle="Solid" OnClick="eli_Click" />
            </td>   
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:TextBox ID="bus" runat="server" Height="25px" Width="132px" TextMode="Number"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="buscar" runat="server" Text="Buscar" Height="48px" Width="148px" BackColor="Aqua" BorderStyle="Solid" OnClick="buscar_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvVe" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />

    </asp:GridView>
</asp:Content>
