﻿using diproma.Clases;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace diproma.Paginas
{
    public partial class crudvehiculos : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from vehiculo";
           // gvVe.DataSource = conn.Dset(cred);
            //gvVe.DataBind();
            if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla() {
            string cred = "Select * from vehiculo";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar() {
            bus.Text = "";
            placa.Text = "";
            modelo.Text = "";
            chasis.Text = "";
            marca.Text = "";
            motor.Text = "";
        }

        protected void crea_Click(object sender, EventArgs e)
        {
            string pl = ""+placa.Text;
            int mt = Convert.ToInt32(motor.Text);
            string ch = ""+chasis.Text;
            string mr = "" +marca.Text;
            int md = Convert.ToInt32(modelo.Text);
            string a = "select * from vehiculo where placa='"+pl+"'";
            if (conn.busqueda(a).Rows.Count > 0) {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Vehiculo ya registrado')", true);
            }
            else {
                string llenar1 = " insert into vehiculo(placa,motor,chasis,marca,modelo) values('"+pl+"',"+mt+",'"+ch+"','"+mr+"',"+md+" )";
                try
                {
                    conn.Operacion(llenar1);
                    limpiar();
                    datatabla();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                }
                catch
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro Vehiculo')", true);
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else 
            {
                string pl = "" + placa.Text;
                int mt = Convert.ToInt32(motor.Text);
                string ch = "" + chasis.Text;
                string mr = "" + marca.Text;
                int md = Convert.ToInt32(modelo.Text);
                string bus1 = ""+bus.Text;
                if (bus1.Equals(pl))
                {
                    string llenar1 = "Update vehiculo SET motor="+mt+" , chasis='"+ch+"', marca='"+mr+"' , modelo='"+md+"' where placa='"+bus1+"' ";
                    try
                    {
                        conn.Operacion(llenar1);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro Vehiculo')", true);
                    }
                }
                else
                {
                    string a = "select * from vehiculo where placa='" + pl + "'";
                    if (conn.busqueda(a).Rows.Count > 0)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Vehiculo ya registrado')", true);
                    }
                    else {
                        string llenar1 = "  update vehiculo set placa='"+pl+"' , motor=" + md + " , chasis='" + ch + "', marca='" + mr + "' , modelo='" + md + "' where placa='" + bus1 + "' ";
                        try
                        {
                            conn.Operacion(llenar1);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro Vehiculo')", true);
                        }
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                string idbusqueda = ""+bus.Text;
                string vtempleado = " select placa from kilometraje where placa='" + idbusqueda + "' ";
                if (conn.busqueda(vtempleado).Rows.Count > 0)
                {//credencial registrada en otra tabla
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error /n Para borrar tiene que borar el registro de Kilometraje, vuelva a intentar')", true);
                }
                else
                {
                    string eliminar = "Delete from vehiculo where placa='" + idbusqueda + "'";
                    try
                    {
                        conn.Operacion(eliminar);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error la placa no esta registrada')", true);
                    }
                }
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            string idbusqueda = ""+bus.Text;
            string vv = "select * from vehiculo where placa='" + idbusqueda + "' ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from vehiculo where placa='" + idbusqueda + "'  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    placa.Text = sdr["placa"].ToString();
                    motor.Text = sdr["motor"].ToString();
                    chasis.Text = sdr["chasis"].ToString();
                    modelo.Text = sdr["modelo"].ToString();
                    marca.Text = sdr["marca"].ToString();
                }

            }
            else
            {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }
    }
}