﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using diproma.Clases;

namespace diproma.Paginas
{
    public partial class crudasigvehiculo : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from asignacion_vehiculo";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
            if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla()
        {
            string cred = "Select * from asignacion_vehiculo";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar()
        {
            bus.Text = "";
            ide.Text = "";
        }
        protected void crea_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(ide.Text);
            int f = Convert.ToInt32(fk.SelectedValue);
            int m = Convert.ToInt32(em.SelectedValue);
            string a1 = "select * from asignacion_vehiculo where id_asig=" + id + " ";
            if (conn.busqueda(a1).Rows.Count > 0)//kilometraje y mes y placa
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Ide, ya registrado')", true);
            }
            else
            {
                string llenar1 = " insert into asignacion_vehiculo(id_asig,fecha,nit) values(" + id + " , " + f + " , " + m + " )";
                try
                {
                    conn.Operacion(llenar1);
                    limpiar();
                    datatabla();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                }
                catch
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro Asignacion')", true);
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int bus1 = Convert.ToInt32(bus.Text);
                int id = Convert.ToInt32(ide.Text);
                int f = Convert.ToInt32(fk.SelectedValue);
                int m = Convert.ToInt32(em.SelectedValue);
                if (bus1 == id)
                {
                        string llenar2 = "Update asignacion_vehiculo SET  fecha=" + f + " , nit="+m+"  where id_asig=" + bus1 + " ";
                        try
                        {
                            conn.Operacion(llenar2);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado kilometaje')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al agregar nuevo kilometraje')", true);
                        }
                }
                else
                {
                    string a1 = "select * from asignacion_vehiculo where id_asig=" + id + " ";
                    if (conn.busqueda(a1).Rows.Count > 0)//kilometraje y mes y placa
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Ide, ya registrado')", true);
                    }
                    else
                    {
                        string llenar2 = "Update asignacion_vehiculo SET  id_asig="+id+" , fecha=" + f + " , nit=" + m + "  where id_asig=" + bus1 + " ";
                        try
                        {
                            conn.Operacion(llenar2);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado kilometaje')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al agregar nuevo kilometraje')", true);
                        }
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
            int idbusqueda = Convert.ToInt32(bus.Text);
                    string eliminar = "Delete from asignacion_vehiculo where id_asig=" + idbusqueda + " ";
                    try
                    {
                        conn.Operacion(eliminar);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error la placa no esta registrada')", true);
                    }
                
            }
        }
        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from asignacion_vehiculo where id_asig=" + idbusqueda + " ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from asignacion_vehiculo where id_asig=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    ide.Text = sdr["id_asig"].ToString();
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }
    }
}