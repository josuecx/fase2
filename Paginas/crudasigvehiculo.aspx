﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crudasigvehiculo.aspx.cs" Inherits="diproma.Paginas.crudasigvehiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Asignacion de Vehiculo
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    Asginacion de Vehiculo
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">
            <li><a href="crudvehiculos.aspx">CRUD Vehiculo</a></li>
            <li><a href="crudkilometraje.aspx">CRUD Kilometraje</a></li>
            <li><a href="crudasigvehiculo.aspx">Asignacion vehiculos</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">
    <table style="width: 131%; height: 376px;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">Asignacion_vehiculo</h3></th>
        </tr>
        <tr>
            <td style="width: 260px">
                <label class="d1">Identificador de asiganacion:</label>
            </td>
            <td style="width: 168px">
                <asp:TextBox ID="ide" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="ide" Display="Dynamic" Text="* llenar campo"></asp:RequiredFieldValidator>
            </td>
            
        </tr>
        <tr>
            <td style="width: 260px">
                <label class="d1">Fecha de Kilometraje:</label>
            </td>
            <td style="width: 168px">
                <asp:DropDownList ID="fk" runat="server" DataSourceID="kilo" DataTextField="id_kilometraje" DataValueField="id_kilometraje" Width="126px"></asp:DropDownList>
                <asp:SqlDataSource ID="kilo" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT [id_kilometraje] FROM [kilometraje]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
           <td style="width: 260px">
                <label class="d1">Empleado a cargo:</label>
            </td>
            <td style="width: 168px">
                <asp:DropDownList ID="em" runat="server" DataSourceID="nit" DataTextField="nit" DataValueField="nit" Height="16px" Width="122px"></asp:DropDownList>
                <asp:SqlDataSource ID="nit" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT [nit] FROM [empleado]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 260px">
                <asp:Button ID="crea" runat="server" Text="Crear" Height="36px" Width="188px" BackColor="Lime" BorderStyle="Solid" OnClick="crea_Click" />
            </td>
            <td style="width: 168px">
                <asp:Button ID="mo" runat="server" Text="Modificar" Height="39px" Width="167px" BackColor="#CC0099" BorderStyle="Solid" OnClick="mo_Click" />
            </td>
            <td>
                 <asp:Button ID="eli" runat="server" Text="Eliminar" Height="35px" Width="119px" BackColor="Red" BorderStyle="Solid" OnClick="eli_Click" />
            </td>   
        </tr>
        <tr>
            <td style="width: 260px">
                <asp:TextBox ID="bus" runat="server" Height="16px" Width="180px" TextMode="Number"></asp:TextBox>
            </td>
            <td style="width: 168px">
                <asp:Button ID="buscar" runat="server" Text="Buscar" Height="36px" Width="166px" BackColor="Aqua" BorderStyle="Solid" OnClick="buscar_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvVe" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />

    </asp:GridView>
</asp:Content>
