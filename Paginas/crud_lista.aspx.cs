﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using diproma.Clases;
namespace diproma.Paginas
{
    public partial class crud_lista : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from lista_producto";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
            if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla()
        {
            string cred = "Select * from lista_producto";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar()
        {
            codigo.Text = "";
            nombre.Text = "";
            f_inicio.Text = "";
            f_fin.Text = "";
            detalle.Text = "";
        }
        protected void crea_Click(object sender, EventArgs e)
        {
            if (detalle.Text=="") {//error
            
            }
            else { //
                int cod = Convert.ToInt32(codigo.Text);
                string n = nombre.Text;
                string fi = f_inicio.Text;
                string ff = f_fin.Text;
                string dt = detalle.Text;
                string a1 = "select * from lista_producto where codigol=" + cod + " ";
                if (conn.busqueda(a1).Rows.Count > 0)//codigo
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo')", true);
                }
                else 
                {
                    string llenar1 = " insert into lista_producto(codigol,nombre,fecha_inicio,fecha_fin,detalle) values(" + cod + " , '" + n + "' ,'"+fi+"' , '"+ff+"','"+dt+"' )";
                    try
                    {
                        conn.Operacion(llenar1);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro Categoria')", true);
                    }
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int cod = Convert.ToInt32(codigo.Text);
                int bus1 = Convert.ToInt32(bus.Text);
                string n = nombre.Text;
                string fi = f_inicio.Text;
                string ff = f_fin.Text;
                string dt = detalle.Text;
                if (bus1 == cod)
                {
                    string llenar1 = "Update lista_producto SET nombre='" + n + "' , fecha_inicio='"+fi+"' , fecha_fin='"+ff+"' , detalle='"+dt+"' where codigol=" + bus1 + "";
                    try
                    {
                        conn.Operacion(llenar1);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de modificar categoria')", true);
                    }
                }
                else
                {
                    string a1 = "select * from lista_producto where codigol=" + cod + " ";
                    if (conn.busqueda(a1).Rows.Count > 0)//kilometraje y mes y placa
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo de categoria, ya registrado')", true);
                    }
                    else
                    {
                        string llenar2 = "Update lista_producto SET  codigol=" + cod + " , nombre='" + n + "' , fecha_inicio='" + fi + "' , fecha_fin='" + ff + "' , detalle='" + dt + "' where codigol=" + bus1 + "  ";
                        try
                        {
                            conn.Operacion(llenar2);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado producto')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al agregar nuevo producto')", true);
                        }
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int idbusqueda = Convert.ToInt32(bus.Text);
                    string eliminar = "Delete from lista_producto where codigol=" + idbusqueda + " ";
                    try
                    {
                        conn.Operacion(eliminar);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error la placa no esta registrada')", true);
                    }
                
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from lista_producto where codigol=" + idbusqueda + " ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from lista_producto where codigol=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    nombre.Text = sdr["nombre"].ToString();
                    codigo.Text = sdr["codigol"].ToString();
                    f_fin.Text = sdr["fecha_fin"].ToString();
                    detalle.Text = sdr["detalle"].ToString();
                    f_inicio.Text = sdr["fecha_inicio"].ToString();
                }
            }
            else
            {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }

        protected void add_producto_Click(object sender, EventArgs e)
        {
            
            if (precio.Text == "") { //error

            }
            else {
                if (detalle.Text=="") {
                    string pr = precio.Text;
                    string pro = producto.SelectedItem.Text;
                    string[] sep = pro.Split('-');
                    detalle.Text = ""+sep[0]+","+pr+";";
                }
                else{
                    string pr = precio.Text;
                    string pro = producto.SelectedItem.Text;
                    string[] sep = pro.Split('-');
                    string datoanterior = detalle.Text;
                    detalle.Text = ""+datoanterior+""+sep[0]+","+pr+"@";
                }
            }
        }
    }
}