﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="ver_empleado.aspx.cs" Inherits="diproma.Paginas.ver_empleado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Ver Empleado
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    Ver Empleado
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="submenu" runat="server">
    
             <li><a href="crear_empleado.aspx">CRUD Empleado</a></li>
            <li><a href="ver_empleado.aspx">Ver Empleado</a></li>
            <li><a href="cargaempleado.aspx">Cargar Empleado</a></li>
            <li><a href="asigempleado.aspx">Asignar Empleados</a></li>
            <li><a href="crud_credencial.aspx">CRUD Credenciales</a></li>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cuadro1" runat="server">
    <asp:GridView ID="gvEmpleado" runat="server" style="width:30px; height:5px;" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
        
        <AlternatingRowStyle BackColor="#CCCCCC" />
        <FooterStyle BackColor="#CCCCCC" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#808080" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />

    </asp:GridView>
</asp:Content>
