﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using diproma.Clases;

namespace diproma.Paginas
{
    public partial class crud_depto : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        protected void Page_Load(object sender, EventArgs e)
        {
            string cred = "Select * from departamento";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
            if (!IsPostBack)
            {
                gvVe.DataSource = conn.Dset(cred);
                gvVe.DataBind();
            }
        }
        public void datatabla()
        {
            string cred = "Select * from departamento";
            gvVe.DataSource = conn.Dset(cred);
            gvVe.DataBind();
        }
        public void limpiar()
        {
            codigo.Text = "";
            nombre.Text = "";
        }
        protected void crea_Click(object sender, EventArgs e)
        {
            int cod = Convert.ToInt32(codigo.Text);
            string n = nombre.Text;
            string a1 = "select * from departamento where codigo=" + cod + " ";
            if (conn.busqueda(a1).Rows.Count > 0)//codigo
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo')", true);
            }
            else
            {
                string llenar1 = " insert into departamento(codigo,nombre) values(" + cod + " , '" + n + "')";
                try
                {
                    conn.Operacion(llenar1);
                    limpiar();
                    datatabla();
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Creado')", true);
                }
                catch
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro departamento')", true);
                }
            }
        }

        protected void mo_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int cod = Convert.ToInt32(codigo.Text);
                int bus1 = Convert.ToInt32(bus.Text);
                string n = nombre.Text;
                if (bus1 == cod)
                {
                    string llenar1 = "Update departamento SET nombre='" + n + "'  where codigo=" + bus1 + "";
                    try
                    {
                        conn.Operacion(llenar1);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de modificar departamento')", true);
                    }
                }
                else
                {
                    string a1 = "select * from departamento where codigo=" + cod + " ";
                    if (conn.busqueda(a1).Rows.Count > 0)//kilometraje y mes y placa
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error Codigo de departamento, ya registrado')", true);
                    }
                    else
                    {
                        string llenar2 = "Update departamento SET  codigo=" + cod + " , nombre='" + n + "'  where codigo=" + bus1 + "  ";
                        try
                        {
                            conn.Operacion(llenar2);
                            limpiar();
                            datatabla();
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se Modificado departamento')", true);
                        }
                        catch
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error al agregar nuevo departamento')", true);
                        }
                    }
                }
            }
        }

        protected void eli_Click(object sender, EventArgs e)
        {
            if (bus.Text == "")
            {
                //error
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error realizar busqueda primero')", true);
            }
            else
            {
                int idbusqueda = Convert.ToInt32(bus.Text);
                //restriccion de lista
                string vtempleado = " select cod_depto from ciudad where cod_depto=" + idbusqueda + " ";
                if (conn.busqueda(vtempleado).Rows.Count > 0)
                {//credencial registrada en otra tabla
                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error /n Para borrar tiene que borar el registro de ciudad, vuelva a intentar')", true);
                }
                else
                {
                    string eliminar = "Delete from departamento where codigo=" + idbusqueda + " ";
                    try
                    {
                        conn.Operacion(eliminar);
                        limpiar();
                        datatabla();
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha eliminado correctamente')", true);
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error ciudad')", true);
                    }
                }
            }
        }

        protected void buscar_Click(object sender, EventArgs e)
        {
            int idbusqueda = Convert.ToInt32(bus.Text);
            string vv = "select * from departamento where codigo=" + idbusqueda + " ";
            if (conn.busqueda(vv).Rows.Count > 0)
            {
                // string buscc = " Select * from credencial where id_credencial=" + idbusqueda + " ";
                SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
                conexion.Open();
                SqlCommand cmd = new SqlCommand("Select * from departamento where codigo=" + idbusqueda + "  ", conexion);
                //adaptador
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                //objeto tabla para leer
                SqlDataReader sdr = cmd.ExecuteReader();
                while (sdr.Read())
                {
                    nombre.Text = sdr["nombre"].ToString();
                    codigo.Text = sdr["codigo"].ToString();
                }
            }
            else
            {
                // esta la persona
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' El ide que busca no existe, vuelva a intentar')", true);
                bus.Text = "";
            }
        }

        protected void busarchivo_Click(object sender, EventArgs e)
        {

        }
    }
}