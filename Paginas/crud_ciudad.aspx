﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crud_ciudad.aspx.cs" Inherits="diproma.Paginas.crud_ciudad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">Crud Ciudad
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    Crud Ciudad
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">
            <li><a href="crud_cliente.aspx">Cliente</a></li>
            <li><a href="crud_ciudad.aspx">Ciudad</a></li>
            <li><a href="crud_depto.aspx">Departamento</a></li>
            <li><a href="crud_meta.aspx">Metas</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">
    <asp:GridView runat="server" style="width:30px; height:5px; position:absolute; top: 3%; left: 60%;" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="codigo" DataSourceID="depaa" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:BoundField DataField="codigo" HeaderText="codigo" ReadOnly="True" SortExpression="codigo" />
            <asp:BoundField DataField="nombre" HeaderText="nombre" SortExpression="nombre" />
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
    </asp:GridView>

    <asp:SqlDataSource ID="depaa" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT * FROM [departamento]"></asp:SqlDataSource>

    <table style="width: 108%; height: 366px;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">CRUD Ciudad</h3></th>
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Codigo de Ciudad:</label>
            </td>
            <td>
                <asp:TextBox ID="codigo" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="codigo" Display="Dynamic" Text="* llenar campo"></asp:RequiredFieldValidator>
            </td>  
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Nombre de la Ciudad:</label>
            </td>
            <td>
                <asp:TextBox ID="nombre" runat="server"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td style="width: 167px">
                <label class="d1">categoria:</label>
            </td>
            <td>
                <asp:DropDownList ID="depto" runat="server" DataSourceID="de" DataTextField="codigo" DataValueField="codigo" Height="38px" Width="122px"></asp:DropDownList>
                <asp:SqlDataSource ID="de" runat="server" ConnectionString="<%$ ConnectionStrings:dipromaConnectionString %>" SelectCommand="SELECT [codigo] FROM [departamento]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:Button ID="crea" runat="server" Text="Crear" Height="54px" Width="154px" BackColor="Lime" BorderStyle="Solid" OnClick="crea_Click" />
            </td>
            <td>
                <asp:Button ID="mo" runat="server" Text="Modificar" Height="47px" Width="153px" BackColor="#CC0099" BorderStyle="Solid" OnClick="mo_Click" />
            </td>
            <td>
                 <asp:Button ID="eli" runat="server" Text="Eliminar" Height="51px" Width="104px" BackColor="Red" BorderStyle="Solid" OnClick="eli_Click" />
            </td>   
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:TextBox ID="bus" runat="server" Height="25px" Width="132px" TextMode="Number"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="buscar" runat="server" Text="Buscar" Height="48px" Width="148px" BackColor="Aqua" BorderStyle="Solid" OnClick="buscar_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvVe" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />

    </asp:GridView>
</asp:Content>
