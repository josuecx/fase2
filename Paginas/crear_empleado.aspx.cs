﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Data.SqlClient;
using diproma.Clases;
using System.Web.UI.WebControls;
using System.EnterpriseServices.Internal;
//using System.Web.UI.Page.UnobtrusiveValidationMode;
//C:\Users\jose antonio cux .LAPTOP-29ACJJUH\Documents\Proyectos\IPC2\Vacaciones\diproma\Global.asax
namespace diproma.Paginas
{
    public partial class crear_empleado : System.Web.UI.Page
    {
        Conexion conn = new Conexion();
        private static string nombre1 = "";
        private static int gnit;
        private static string gapellido;
        private static string gfecha;
        private static int gcelular;
        private static int gtelefono;
        private static int gpuesto;
        private static string gemail;
        private static string gpass;
        public static string Nombre1 { get => nombre1; set => nombre1 = value; }
        public static int Gnit { get => gnit; set => gnit = value; }
        public static string Gapellido { get => gapellido; set => gapellido = value; }
        public static string Gpass { get => gpass; set => gpass = value; }
        public static int Gcelular { get => gcelular; set => gcelular = value; }
        public static string Gfecha { get => gfecha; set => gfecha = value; }
        public static int Gtelefono { get => gtelefono; set => gtelefono = value; }
        public static int Gpuesto { get => gpuesto; set => gpuesto = value; }
        public static string Gemail { get => gemail; set => gemail = value; }
        public static string Gdomicilio { get => gdomicilio; set => gdomicilio = value; }
        public static int Gnitbus { get => gnitbus; set => gnitbus = value; }

        private static string gdomicilio;

        private static int gnitbus;

        protected void Page_Load(object sender, EventArgs e)
        {
            
           

        }
        
        protected void crear_Click(object sender, EventArgs e)
        {
            //variables 
            int nit1 = Convert.ToInt32(nit.Text); //nit de empleado   not null   llave
            string nombres1 = "" + nombres.Text;
            string apellido1 = "" + apellidos.Text;
            string fecha_nac1 = "" + fecha_nac.Text;
            string pass1 = "" + pass.Text;
            string email1 = "" + email.Text;
            string domicilio1 = "" + domicilio.Text;
            int puesto = Convert.ToInt32(credencial.SelectedValue);// puesto del empleado not null
                    string comparacion = "Select * from empleado where nit="+nit1+" and email='"+email1+"'";
                    string solonit = "Select* from empleado where nit=" + nit1 + " ";
                    string soloemail = " Select * from empleado where email='" + email1 + "' ";
                    if (conn.busqueda(comparacion).Rows.Count > 0)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Usuario ya registrado, vuelva a intentar')", true); 
                    }
                    else if (conn.busqueda(solonit).Rows.Count > 0)
                    {
                         ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error nit ya registrado')", true);
                    }
                    else if (conn.busqueda(soloemail).Rows.Count > 0)
                    {
                         ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error correo ya resgitrado')", true);
                    }
                    else
                    {
                        //SIN-TELFENOS
                        if (telefono_d.Text == "" && celular.Text == "") {// si no tienen telefono 
                            string sinmovil = "insert into empleado(nit,nombres,apellidos,fecha_nacimiento,domicilio,email,pass,puesto) " +
                                              " Values(" + nit1 + ",'" + nombres1 + "','" + apellido1 + "','" + fecha_nac1 + "','" + domicilio1 + "','" + email1 + "','" + pass1 + "'," + puesto + ")";
                            try
                            {
                                conn.Operacion(sinmovil);
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha registrado correctamente')", true);
                            }
                            catch
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de registro sin movil')", true);
                            }
                        //SIN TELFONOS
                        } else if (telefono_d.Text == "") {
                            //con celular
                            int celular1 = Convert.ToInt32(celular.Text);// celular
                            string celularaa = "insert into empleado(nit,nombres,apellidos,fecha_nacimiento,domicilio,celular,email,pass,puesto) " +
                                           " Values(" + nit1 + ",'" + nombres1 + "','" + apellido1 + "','" + fecha_nac1 + "','" + domicilio1 + "'," + celular1 + ",'" + email1 + "','" + pass1 + "'," + puesto + ")";
                            try
                            {
                                conn.Operacion(celularaa);
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha registrado correctamente')", true);
                            }
                            catch
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error celular')", true);
                            }
                        //Con celular
                        } else if (celular.Text=="") { 
                            //con telefono
                            int tel = Convert.ToInt32(telefono_d.Text);// telefono 
                            string tele = "insert into empleado(nit,nombres,apellidos,fecha_nacimiento,domicilio,telefono,email,pass,puesto) " +
                                               " Values(" + nit1 + ",'" + nombres1 + "','" + apellido1 + "','" + fecha_nac1 + "','" + domicilio1 + "'," + tel + ",'" + email1 + "','" + pass1 + "'," + puesto + ")";
                            try
                            {
                                conn.Operacion(tele);
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha registrado correctamente')", true);
                            }
                            catch
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error de telefono')", true);
                            }
                        //Con telefono
                        }
                        else
                        {
                            int tel = Convert.ToInt32(telefono_d.Text);// telefono 
                            int cel = Convert.ToInt32(celular.Text);
                            string smart = "insert into empleado(nit,nombres,apellidos,fecha_nacimiento,domicilio,telefono,celular,email,pass,puesto) " +
                                      " Values(" + nit1 + ",'" + nombres1 + "','" + apellido1 + "','" + fecha_nac1 + "','" + domicilio1 + "'," + tel + "," + cel + ",'" + email1 + "','" + pass1 + "'," + puesto + ")";
                            try
                            {
                                conn.Operacion(smart);
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha registrado correctamente')", true);
                            }
                            catch
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error varios')", true);
                            }
                        }
                    }
        }
        protected void mo_Click(object sender, EventArgs e)
        {
            Gnit = Convert.ToInt32(nit.Text);//nit 
            Gemail = email.Text;//email
            Nombre1 = nombres.Text;//nombre
            Gapellido = apellidos.Text;//apellido
            Gfecha = fecha_nac.Text;//fecha
            Gpass = pass.Text;
            Gnitbus = Convert.ToInt32(busqueda_nit.Text);
            if (busqueda_nit.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' No ha realizado la busqueda')", true);
            }
            else
            {
                string comparacion = "Select * from empleado where nit=" + Gnit + " and email='" + Gemail + "'";
                string solonit = "Select* from empleado where nit=" + Gnit + " ";
                string soloemail = " Select * from empleado where email='" + Gemail + "' ";
                int cop = 0;
                if (conn.busqueda(comparacion).Rows.Count > 0)// si son iguales 
                {
                    cop = 1;
                }
                else if (conn.busqueda(solonit).Rows.Count > 0)
                {
                    cop = 2;
                }
                else if (conn.busqueda(soloemail).Rows.Count > 0)
                {
                    cop = 3;
                }
                else
                {
                    cop = 4;
                }
                if (cop==1) {
                    if (telefono_d.Text == "" && celular.Text == "")
                    {
                        string sinmovil = "UPDATE empleado SET nombres='" + Nombre1 + "' , apellidos='" + Gapellido + "'  where nit=" + Gnitbus + " ";
                        try { conn.Operacion(sinmovil); ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha Modificado correctamente')", true); }
                        catch { ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Error de Modificacion sin movil nit y email')", true); }
                        //SIN TELFONOS
                    }
                    else if (telefono_d.Text == "")
                    {
                        //con celular
                        Gcelular = Convert.ToInt32(celular.Text);// celular
                        string celularaa = "Update empleado set celular=" + Gcelular + " , nombres='" + Nombre1 + "' , apellidos='" + Gapellido + "' , fecha_nacimiento='" + Gfecha + "' , domicilio='" + Gdomicilio + "' , pass='" + Gpass + "' , puesto=" + Gpuesto + " where nit=" + Gnitbus + " ";
                        try { conn.Operacion(celularaa); ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha Modificado correctamente')", true); }
                        catch { ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error celular nit y email')", true); }
                        //Con celular
                    }
                    else if (celular.Text == "")
                    {
                        //con telefono
                        Gtelefono = Convert.ToInt32(telefono_d.Text);// telefono 
                        string tele = "Update empleado set telefono=" + Gtelefono + " , nombres='" + Nombre1 + "', apellidos='" + Gapellido + "', fecha_nacimiento='" + Gfecha + "', domicilio='" + Gdomicilio + "', pass='" + Gpass + "', puesto=" + Gpuesto + " where nit=" + Gnitbus + " ";
                        try { conn.Operacion(tele); ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha Modificado correctamente')", true); }
                        catch { ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error de telefono nit y email')", true); }
                        //Con telefono
                    }
                    else
                    {
                        Gtelefono = Convert.ToInt32(telefono_d.Text);// telefono 
                        Gcelular = Convert.ToInt32(celular.Text);
                        string smart = "Update empleado set celular=" + Gcelular + ", telefono=" + Gtelefono + ", nombres='" + Nombre1 + "', apellidos='" + Gapellido + "', fecha_nacimiento='" + Gfecha + "', domicilio='" + Gdomicilio + "', pass='" + Gpass + "', puesto=" + Gpuesto + " where nit=" + Gnitbus + " ";
                        try { conn.Operacion(smart); ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha Modificado correctamente')", true); }
                        catch { ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error varios nit y email')", true); }
                    }
                }
            }
        }
        protected void eli_Click(object sender, EventArgs e)
        {
            if (busqueda_nit.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' No ha realizado la busqueda')", true);
            }
            else
            {
                string celularaa = "Update empleado set celular=" + Gcelular + " , nombres='" + Nombre1 + "' , apellidos='" + Gapellido + "' , fecha_nacimiento='" + Gfecha + "' , domicilio='" + Gdomicilio + "' , pass='" + Gpass + "' , puesto=" + Gpuesto + " where nit=" + Gnitbus + " ";
                try { conn.Operacion(celularaa); ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Se ha Modificado correctamente')", true); }
                catch { ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert(' Error celular nit y email')", true); }

            }

        }
        
        protected void bus1_Click(object sender, EventArgs e)
        {
            int buscar = Convert.ToInt32(buscarnit.SelectedValue);
            busqueda_nit.Text = ""+buscar;
            string buscc = " Select * from empleado where nit=" + buscar + " ";
            SqlConnection conexion = new SqlConnection("Data Source=localhost\\SQLEXPRESS;Initial Catalog=diproma; Integrated Security=True");
            conexion.Open();
            SqlCommand cmd = new SqlCommand("Select * from empleado where nit=" + buscar + " ", conexion);
            //adaptador
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            //objeto tabla para leer
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read()) {
                nit.Text = sdr["nit"].ToString();
                nombres.Text = sdr["nombres"].ToString();
                apellidos.Text = sdr["apellidos"].ToString();
                fecha_nac.Text= sdr["fecha_nacimiento"].ToString();
                pass.Text = sdr["pass"].ToString();
                email.Text = sdr["email"].ToString();
                domicilio.Text = sdr["domicilio"].ToString();
                domicilio.Text = sdr["domicilio"].ToString();
                
            }
        }
    }
} 