﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crud_categoria.aspx.cs" Inherits="diproma.Paginas.crud_categoria" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    CRUD Categoria
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    CRUD Categoria
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">
    
            <li><a href="crud_producto.aspx">CRUD Producto</a></li>
            <li><a href="crud_categoria.aspx">CRUD Categoria</a></li>
            <li><a href="crud_lista.aspx">CRUD Lista</a></li>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">

     <table style="width: 108%; height: 366px;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">CRUD Categoria</h3></th>
        </tr>
        <tr>
            <td style="width: 167px"><label class="d1">Codigo de Categoria:</label></td>
            <td>
                <asp:TextBox ID="codigo" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="codigo" Display="Dynamic" Text="* llenar campo"></asp:RequiredFieldValidator>
            </td>  
        </tr>
        <tr>
            <td style="width: 167px">
                <label class="d1">Categoria:</label>
            </td>
            <td>
                <asp:TextBox ID="nombre" runat="server" Height="22px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="nombre" Display="Dynamic" ErrorMessage="RequiredFieldValidator">*Llenar campo</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 167px">
                <asp:Button ID="crear" runat="server" Height="40px" Text="Crear" Width="142px" BackColor="Lime" BorderStyle="Solid" OnClick="crear_Click" />
            </td>
            <td>
                <asp:Button ID="mo" runat="server" Text="Modificar" Height="47px" Width="153px" BackColor="#CC0099" BorderStyle="Solid" OnClick="mo_Click" />
            </td>
            <td>
                 <asp:Button ID="eli" runat="server" Text="Eliminar" Height="51px" Width="104px" BackColor="Red" BorderStyle="Solid" OnClick="eli_Click" />
            </td>   
        </tr>
        <tr>
            <td>
               <asp:TextBox ID="bus" runat="server" Height="30px" Width="132px" TextMode="Number"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="buscar" runat="server" Text="Buscar" Height="48px" Width="148px" BackColor="Aqua" BorderStyle="Solid" OnClick="buscar_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvVe" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />

    </asp:GridView>
</asp:Content>
