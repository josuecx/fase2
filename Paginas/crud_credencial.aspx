﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crud_credencial.aspx.cs" Inherits="diproma.Paginas.crud_credencial" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    CRUD Credencial
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    CRUD Credencial
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">
    
            <li><a href="../Paginas/crear_empleado.aspx">CRUD Empleado</a></li>
            <li><a href="../Paginas/ver_empleado.aspx">Ver Empleado</a></li>
            <li><a href="cargaempleado.aspx">Cargar Empleado</a></li>
            <li><a href="asigempleado.aspx">Asignar Empleados</a></li>
            <li><a href="crud_credencial.aspx">CRUD Credenciales</a></li>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">
    <table id="crud" style="width: 117%;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">CRUD Credencial</h3></th>
        </tr>
        <tr>
            <td class="auto-style37" style="width: 252px"><label class="d1">id_credencial:</label></td>
            <td>
                <asp:TextBox ID="id_cre" runat="server" TextMode="Number"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Error" ControlToValidate="id_cre" Display="Dynamic" Text="* Solo numero"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <label class="d1">Nombre Credencial:</label>
            </td>
            <td>
                <asp:TextBox ID="ncre" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Error" ControlToValidate="ncre" Display="Dynamic" Text="* Llenar campo"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <table id="operaciones" style="width: 100%;">
        
        <tr>
            <th id="ta" colspan="2" style="height: 10px"><h5 style="height: 10px">Operaciones</h5></th>
        </tr>
        <tr>
            <td>
                <asp:Button ID="cre" runat="server" Text="Crear Credencial" BackColor="#00CC00" BorderStyle="Solid" Height="40px" OnClick="cre_Click" Width="162px" />
            </td>
            <td style="width: 256px">
                <asp:Button ID="mo" runat="server" Text="Modificar Credencial" BackColor="#CC0099" BorderStyle="Solid" Height="40px" OnClick="mo_Click" Width="183px" />
            </td>
            <td>
                <asp:Button ID="eli" runat="server" Text="Eliminar Credencial" BackColor="Red" BorderStyle="Solid" Height="40px" OnClick="eli_Click" Width="169px" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="bus" runat="server" TextMode="Number"></asp:TextBox>
            </td>
            <td style="width: 256px">
                <asp:Button ID="buscar" runat="server" Text="Buscar Credencial" BackColor="Aqua" BorderStyle="Solid" Height="52px" OnClick="buscar_Click" Width="125px" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvC" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
    </asp:GridView>
</asp:Content>
