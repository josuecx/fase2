﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="crudvehiculos.aspx.cs" Inherits="diproma.Paginas.crudvehiculos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    CRUD Vehiculo
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    CRUD Vehiculo
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">

            <li><a href="crudvehiculos.aspx">CRUD Vehiculo</a></li>
            <li><a href="crudkilometraje.aspx">CRUD Kilometraje</a></li>
            <li><a href="crudasigvehiculo.aspx">Asignacion vehiculos</a></li>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">
    <table style="width: 100%;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">CRUD Vehiculo</h3></th>
        </tr>
        <tr>
            <td>
                <label class="d1">Placa:</label>
            </td>
            <td>
                <asp:TextBox ID="placa" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="placa" Display="Dynamic" Text="* llenar campo"></asp:RequiredFieldValidator>
            </td>
            
        </tr>
        <tr>
            <td>
                <label class="d1">Motor:</label>
            </td>
            <td>
                <asp:TextBox ID="motor" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="motor" Display="Dynamic" Text="* Solo numero"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
           <td>
                <label class="d1">Chasis:</label>
            </td>
            <td>
                <asp:TextBox ID="chasis" runat="server"></asp:TextBox>
            </td>
        </tr>
          <tr>
            <td>
                <label class="d1">Marca:</label>
            </td>
            <td>
                <asp:TextBox  ID="marca" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
           <td>
                <label class="d1">Modelo:</label>
            </td>
            <td>
                <asp:TextBox ID="modelo" runat="server" TextMode="Number"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="modelo" Display="Dynamic" Text="* Solo numero"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="crea" runat="server" Text="Crear" Height="36px" Width="121px" BackColor="Lime" BorderStyle="Solid" OnClick="crea_Click" />
            </td>
            <td>
                <asp:Button ID="mo" runat="server" Text="Modificar" Height="32px" Width="151px" BackColor="#CC0099" BorderStyle="Solid" OnClick="mo_Click" />
            </td>
            <td>
                 <asp:Button ID="eli" runat="server" Text="Eliminar" Height="30px" Width="97px" BackColor="Red" BorderStyle="Solid" OnClick="eli_Click" />
            </td>   
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="bus" runat="server" Height="25px" Width="132px"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="buscar" runat="server" Text="Buscar" Height="37px" Width="149px" BackColor="Aqua" BorderStyle="Solid" OnClick="buscar_Click" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="gvVe" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
        <AlternatingRowStyle BackColor="#F7F7F7" />
        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
        <SortedAscendingCellStyle BackColor="#F4F4FD" />
        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
        <SortedDescendingCellStyle BackColor="#D8D8F0" />
        <SortedDescendingHeaderStyle BackColor="#3E3277" />

    </asp:GridView>
</asp:Content>
