﻿<%@ Page Title="" Language="C#" MasterPageFile="~/web_master/Modulo_admin.Master" AutoEventWireup="true" CodeBehind="asigempleado.aspx.cs" Inherits="diproma.Paginas.asigempleado" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    Asignacion de jefe empleado
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="titulo1" runat="server">
    Asignacion de Jefe Empleado
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="submenu" runat="server">
            <li><a href="crear_empleado.aspx">CRUD Empleado</a></li>
            <li><a href="ver_empleado.aspx">Ver Empleado</a></li>
            <li><a href="cargaempleado.aspx">Cargar Empleado</a></li>
            <li><a href="asigempleado.aspx">Asignar Empleados</a></li>
            <li><a href="crud_credencial.aspx">CRUD Credenciales</a></li>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cuadro1" runat="server">

    <table style="width:100%;">
        <tr>
            <th id="tableth" colspan="2" style="height: 10px"><h3 style="height: 10px">Asignacion de Jefe</h3></th>
        </tr>
        <tr>
            <td class="auto-style37" style="width: 252px"><label class="d1">Nit empleado:</label></td>
            <td>
                <asp:DropDownList ID="nit_empleado" runat="server"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>

</asp:Content>
